[![pipeline status](https://gitlab.com/eguiraud/serializable-yaml/badges/main/pipeline.svg)](https://gitlab.com/eguiraud/serializable-yaml/-/commits/main)
[![crates.io](https://img.shields.io/crates/v/serializable-yaml.svg)](https://crates.io/crates/serializable-yaml)

# Serializable YAML

This library makes [yaml-rust](https://crates.io/crates/yaml-rust)'s YAML enum serializable.

Because of the orphan rule we cannot implement the necessary trait on the YAML enum directly,
so instead this library provides its own serializable equivalent with the same name.

For (de)serialization of custom Rust types to/from YAML see [serde_yaml](https://crates.io/crates/serde_yaml).

## Usage

```rust
// load some YAML with yaml-rust
let yaml = yaml_rust::YamlLoader::load_from_str(input).unwrap();
// convert it to the serializable-yaml equivalent
let yaml = serializable_yaml::from_vec(yaml);
// you can now serialize that YAML instance with serde_yaml
let yaml_as_string = serde_yaml::to_string(&yaml).unwrap();
```

Utility function `from_map` is also available, see [the docs](https://docs.rs/serializable-yaml/0.2.0/serializable_yaml/).

## Known bugs and limitations

Only strings are supported as hashmap keys, no other types.

Please report any other bugs as GitLab issues.
